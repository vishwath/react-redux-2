import React from "react";
import cx from "classnames";
import { connect } from "react-redux";
import { setFilter } from "../actions";
import { VISIBILITY_FILTERS } from "../constants";
import '../styles/filter.scss'

const VisibilityFilters = ({ activeFilter, setFilter }) => {
    return (
        <div className="visibility-filters">
            {Object.keys(VISIBILITY_FILTERS).map(filterKey => {
                const currentFilter = VISIBILITY_FILTERS[filterKey];
                return (
                    <div
                        key={`filter-${currentFilter}`}
                        className={cx(
                            "filter",
                            currentFilter === activeFilter && "filter--active"
                        )}
                        onClick={() => {
                            setFilter(currentFilter);
                        }}
                    >
            { currentFilter }
          </div>
                );
            })}
        </div>
    );
};

const mapStateToProps = state => {
    return { activeFilter: state.visibilityFilter };
};

export default connect(
    mapStateToProps,
    { setFilter }
)(VisibilityFilters);
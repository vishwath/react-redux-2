import React, {useState} from "react";
import {connect} from "react-redux";
import cx from "classnames";
import {deleteTodo, toggleTodo, editTodo} from "../actions";
import '../styles/todo.scss'

const Todo = ({todo, toggleTodo, deleteTodo, editTodo}) => {
    const [editing, setEdit] = useState(false);
    const [task, setTask] = useState(todo.content);
    const handleEditing = () => {
        console.log(editing)
        setEdit(!editing);
    };
    const editHandler = (evt) => {
        evt.preventDefault();
        editTodo(todo.id, task);
        setEdit(!editing);
    }

    let viewMode = {}
    let editMode = {}
    if (editing) {
        viewMode.display = "none"
    } else {
        editMode.display = "none"
    }

    return (
        <div className="todo-box">
            <div
                onClick={() => toggleTodo(todo.id)}
                className={cx(
                    "todo-text",
                    todo && todo.completed && "todo-textCompleted"
                )}
                style={viewMode}>
                {todo.content}
            </div>
            <div className="todo-buttons">
                <button onClick={handleEditing} className="edit-todo" style={viewMode}>Edit</button>
                <button className="delete-todo" onClick={() => deleteTodo(todo.id)} style={viewMode}>Delete</button>
            </div>
            <div>
                <input
                    type="text"
                    style={editMode}
                    value={task}
                    onChange={evt => {
                        setTask(evt.target.value);
                    }}
                />
                <button className="edit-todo" style={editMode} onClick={editHandler}>Edit</button>
            </div>
        </div>
    );
}

export default connect(
    null,
    {toggleTodo, deleteTodo, editTodo}
)(Todo);
import {ADD_TODO, DELETE_TODO, EDIT_TODO, TOGGLE_TODO} from "../actions/actionTypes";

const initialState = {
    allIds: [],
    byIds: {}
};

export default function(state = initialState, action) {
    switch (action.type) {
        case ADD_TODO: {
            const { id, content } = action.payload;
            return {
                ...state,
                allIds: [...state.allIds, id],
                byIds: {
                    ...state.byIds,
                    [id]: {
                        content,
                        completed: false
                    }
                }
            };
        }
        case TOGGLE_TODO: {
            const { id } = action.payload;
            return {
                ...state,
                byIds: {
                    ...state.byIds,
                    [id]: {
                        ...state.byIds[id],
                        completed: !state.byIds[id].completed
                    }
                }
            };
        }
        case DELETE_TODO: {
            const { id } = action.payload;
            let removed;
            return {
                ...state,
                allIds : state.allIds.filter(item => item!==id),
                byIds: {
                    ...state.byIds,
                    [id]: removed
                }
            }
        }
        case EDIT_TODO: {
            const {id,title} = action.payload;
            return {
                ...state,
                byIds: {
                    ...state.byIds,
                    [id]: {
                        ...state.byIds[id],
                        content: title
                    }
                }
            }
        }
        default:
            return state;
    }
}
import {ADD_TODO, TOGGLE_TODO, SET_FILTER, DELETE_TODO, EDIT_TODO} from "./actionTypes";

let nextTodoId = 1;

export const addTodo = content => ({
    type: ADD_TODO,
    payload: {
        id: nextTodoId++,
        content
    }
});

export const toggleTodo = id => ({
    type: TOGGLE_TODO,
    payload: {id}
});

export const deleteTodo = id => ({
    type: DELETE_TODO,
    payload: {id}
})

export const editTodo = (id,title) => ({
    type: EDIT_TODO,
    payload: {id,title}
})

export const setFilter = filter => ({type: SET_FILTER, payload: {filter}});
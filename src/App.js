import './styles/app.scss';
import AddTodo from "./components/todoForm";
import TodoList from "./components/TodoList";
import VisibilityFilters from "./components/Filters";

function App() {
  return (
    <div className="App">
        <h1 className="todo-app">To-do App</h1>
        <VisibilityFilters />
        <TodoList />
        <AddTodo />
    </div>
  );
}

export default App;
